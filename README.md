Author: Ameya Bhandari

Contact: ameyab@uoregon.edu

Description: Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

